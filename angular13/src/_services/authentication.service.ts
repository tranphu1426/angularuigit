import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { error } from "console";
import { BehaviorSubject, Observable, map } from "rxjs";
import { User } from "src/_models/user";

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    private currentUserSubject: BehaviorSubject<User | null>;
    public currentUser: Observable<User | null>;
    readonly APIUrl = "http://127.0.0.1:8000";

    constructor(private http: HttpClient) {
        let storage: any = localStorage.getItem('currentUser');
        let user = JSON.parse(storage);
        this.currentUserSubject = new BehaviorSubject<User | null>(user);
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User | null {
        return this.currentUserSubject.value;
    }

    login(val: any) {

        return this.http.post<any>(this.APIUrl + '/login', val)
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user) {
                    console.log("user", user);
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

}