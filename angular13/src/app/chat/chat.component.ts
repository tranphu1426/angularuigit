import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';
// import { MessageResponse, Channel } from 'stream-chat';
// import { MessageResponse} from 'stream-chat';

@Component({
	selector: 'app-chat',
	templateUrl: './chat.component.html',
	styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

	constructor(private service: SharedService) { }

	messages: any[] = [];
	message = '';

	ngOnInit(): void {
	}

	sendMessage() {
		if (this.message) {

			var val = {
				// QuestionId: '1',
				QuestionText: this.message,
				Employee: 'conan'
			};

			this.messages = this.messages.concat(val);
			this.message = '';

			this.service.chat(val).subscribe(res => {
				console.log(res);
				var result = {
					QuestionText: res,
					Employee: '0'
				};
				this.messages = this.messages.concat(result);
			});
		}

	}
}
