import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { User } from 'src/_models/user';
import { AuthenticationService } from 'src/_services/authentication.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	loginForm!: FormGroup;
	loading = false;
	submitted = false;
	returnUrl: string = "";
	error: string = "";

	constructor(
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private authenticationService: AuthenticationService,
		// private alertService: AlertService
	) {
		// redirect to home if already logged in
		if (this.authenticationService.currentUserValue) {
			this.router.navigate(['/']);
		}
	}

	ngOnInit(): void {
		this.loginForm = this.formBuilder.group({
			email: ['', Validators.required],
			password: ['', Validators.required]
		});

		// get return url from route parameters or default to '/'
		this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
	}

	// convenience getter for easy access to form fields
	get f() { return this.loginForm.controls; }

	onSubmit() {
		this.submitted = true;

		// stop here if form is invalid
		if (this.loginForm.invalid) {
			return;
		}

		this.loading = true;
		// this.authenticationService.login(this.f['email'].value, this.f['password'].value)
		// 	.pipe(first())
		// 	.subscribe(
		// 		data => {
		// 			this.router.navigate([this.returnUrl]);
		// 		},
		// 		error => {
		// 			this.alertService.error(error);
		// 			this.loading = false;
		// 		});
		console.log("this.f", this.f);

		var param = {
			email: this.f['email'].value,
			password: this.f['password'].value
		}

		this.authenticationService.login(param)
			.pipe(first())
			.subscribe(
				{
					next: (data: any) => {
						console.log("data", data);
						console.log("You welcome");
						this.loading = false;
						this.error = "";
						this.router.navigate([this.returnUrl]);
					},
					error: (e: any) => {
						console.log("error", e);
						this.error = "Loi roi!!!";
						this.loading = false;
					}
				}
			);
	}

}
