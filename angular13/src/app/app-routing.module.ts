import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EmployeeComponent } from './employee/employee.component';
import { DepartmentComponent } from './department/department.component';
import { ChatComponent } from './chat/chat.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from 'src/_helpers/auth.guard';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
	{ path: '', component: HomeComponent, canActivate: [AuthGuard] },
	{ path: 'employee', component: EmployeeComponent, canActivate: [AuthGuard] },
	{ path: 'department', component: DepartmentComponent, canActivate: [AuthGuard] },
	{ path: 'chat', component: ChatComponent, canActivate: [AuthGuard] },
	{ path: 'login', component: LoginComponent },

	// otherwise redirect to home
	{ path: '**', redirectTo: '' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
